import java.awt.*;
import java.util.Scanner;

public class Newtask {
    public static void main(String[] args){
//        String nama, alamat;
//        int umur;
//        int tinggibadan;
//
//        nama = "Kimi";
//        alamat = "bsd";
//        umur = 20;
//        tinggibadan = 122;

//        System.out.println("nama : "+ nama);
//        System.out.println("alamat : "+ alamat);
//        System.out.println("umur : "+ umur + "tahun");
//        System.out.println("tinggi badan : "+ tinggibadan + "cm");

        String nama, alamat;
        int usia, gaji;

        //membuat scanner baru
        Scanner keyboard = new Scanner(System.in);

        //Tampilkan output ke user
        System.out.println("### Pendataan Karyawan PT. Todays ###");
        System.out.println("Nama Karyawan: ");

        //menggunakan scanner dan menyimpan apa yang diketik
        nama = keyboard.next();
        //Tampilkan output lagi
        System.out.println("Alamat: ");
        //menggunakan scanner lagi
        alamat = keyboard.next();

        System.out.println("Usia: ");
        usia = keyboard.nextInt();

        System.out.println("Gaji: ");
        gaji = keyboard.nextInt();

        //Menampilkan hasil output
        System.out.println("____________________________");
        System.out.println("Nama Karyawan : " + nama);
        System.out.println("Alamat Karyawan : " + alamat);
        System.out.println("Usia Karyawan : " + usia);
        System.out.println("Gaji Karyawan :Rp " + gaji);
    }
}
