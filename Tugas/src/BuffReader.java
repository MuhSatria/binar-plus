import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BuffReader {
    public static void main (String[] args) throws IOException {

        String nama ;

        //Membuat Object Inputstream
        InputStreamReader isr = new InputStreamReader(System.in);

        //Membuat object Buffreader
        BufferedReader br = new BufferedReader(isr);

        //Mengisi variable nama dengan BufferedReader
        System.out.println("Inputkan Nama : ");
        nama = br.readLine();

        //Tampilkan output isi variable nama
        System.out.println("Nama saya adalah : " + nama);
    }

}
